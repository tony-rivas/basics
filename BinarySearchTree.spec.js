const expect = require('chai').expect
const bST = require('./BinarySearchTree').BST
const log = require('./BinarySearchTree').log

describe ('Binary Search Tree', () => {
    let bst
    beforeEach(() => {
    })

    it ('should create the initial tree', () => {
        bst = new bST(50)
        expect(bst.value).eq(50)
        expect(bst.left).eq(null)
        expect(bst.right).eq(null)
    })

    describe('Insert, contains, search', () => {
        beforeEach(() => {
            bst = new bST(50)
            bst.insert(40)
            bst.insert(60)
            bst.insert(45)
            bst.insert(57)
            bst.insert(100)
            bst.insert(90)
        })

        it ('should insert nodes to via left or right', () => {
            expect(bst.left.value).eq(40)
            expect(bst.left.left).eq(null)
            expect(bst.left.right.value).eq(45)
            expect(bst.right.value).eq(60)
            expect(bst.right.left.value).eq(57)
            expect(bst.right.right.value).eq(100)
            expect(bst.right.right.left.value).eq(90)
            expect(bst.right.right.right).eq(null)
        })
    
        it ('should contain or not contain values', () => {

            expect(bst.contains(83)).eq(false)
            expect(bst.contains(50)).eq(true)
            expect(bst.contains(100)).eq(true)
            expect(bst.contains(45)).eq(true)
        })

        it ('should traverse the tree depth first and in order', () => {
            // how do I unit test this?
            bst.depthFirstTraversal(log, 'in-order')
            bst.depthFirstTraversal(log, 'pre-order')
        })
    })

})