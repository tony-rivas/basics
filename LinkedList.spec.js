const expect = require('chai').expect
const linkedList = require('./LinkedList').LinkedList
const node = require('./LinkedList').Node

describe ('LinkedList', () => {
    let ll
    beforeEach(() => {
        ll = new linkedList()
    })

    it ('should correctly set up a single node list via addToHead', () => {
        ll.addToHead(100)
        expect(ll.head).eql({value: 100, next: null, previous: null})
        expect(ll.tail).eql({value: 100, next: null, previous: null})
    })

    it ('should correctly insert 2 nodes at the beginning of the list', () => {
        ll.addToHead(100)
        ll.addToHead(200)
        expect(ll.head.value).eq(200)
        expect(ll.head.next.value).eq(100)
        expect(ll.head.next.next).eq(null)
        expect(ll.head.next.previous.value).eq(200, 'pointing back at head')
        expect(ll.head.previous).eq(null)
        expect(ll.tail.value).eq(100)
        expect(ll.tail.next).eq(null)
        expect(ll.tail.previous.value).eq(200)
        expect(ll.tail.previous.next.value).eq(100)
        expect(ll.tail.previous.previous).eq(null)
    })

    it ('should correctly set up a single node list via addToTail', () => {
        ll.addToTail(100)
        expect(ll.tail).eql({value: 100, next: null, previous: null})
        expect(ll.head).eql({value: 100, next: null, previous: null})
    })

    it ('should correctly insert 2 nodes at the end of the list', () => {
        ll.addToTail(100)
        ll.addToTail(200)
        expect(ll.tail.value).eq(200)
        expect(ll.tail.next).eq(null)
        expect(ll.tail.previous.value).eq(100)
        expect(ll.tail.previous.previous).eq(null)
        expect(ll.tail.previous.next.value).eq(200, 'pointing back at tail')
        expect(ll.head.value).eq(100)
        expect(ll.head.previous).eq(null)
        expect(ll.head.next.value).eq(200)
        expect(ll.head.next.previous.value).eq(100)
        expect(ll.head.next.next).eq(null)
    })

    it ('should correctly insert 3 nodes, two in the front, and one at the end of the list', () => {
        ll.addToHead(100)
        ll.addToHead(200)
        ll.addToTail(300)
        expect(ll.head.value).eq(200)
        expect(ll.tail.value).eq(300)
        expect(ll.head.next.value).eq(100)
        expect(ll.tail.previous.value).eq(100)
        expect(ll.head.next.next.value).eq(300)
        expect(ll.tail.previous.previous.value).eq(200)
    })

    it ('should return null for removing the head of an empty list', () => {
        const val = ll.removeHead()
        expect(val).eq(null)
        expect(ll.head).eq(null)
        expect(ll.tail).eq(null)
    })

    it ('should return the value for removing the head of a one node list', () => {
        ll.addToHead(100)
        const val = ll.removeHead()
        expect(val).eq(100)
        expect(ll.head).eq(null)
        expect(ll.tail).eq(null)
    })

    it ('should return the value for removing the head of a mutiple node list', () => {
        ll.addToHead(100)
        ll.addToTail(200)
        ll.addToTail(300)
        const val = ll.removeHead()
        expect(val).eq(100)
        expect(ll.head.value).eq(200)
        expect(ll.tail.value).eq(300)
    })

    it ('should return null for removing the tail of an empty list', () => {
        const val = ll.removeTail()
        expect(val).eq(null)
        expect(ll.head).eq(null)
        expect(ll.tail).eq(null)
    })

    it ('should return the value for removing the tail of a one node list', () => {
        ll.addToHead(100)
        const val = ll.removeTail()
        expect(val).eq(100)
        expect(ll.head).eq(null)
        expect(ll.tail).eq(null)
    })

    it ('should return the value for removing the tail of a mutiple node list', () => {
        ll.addToHead(100)
        ll.addToTail(200)
        ll.addToTail(300)
        const val = ll.removeTail()
        expect(val).eq(300)
        expect(ll.head.value).eq(100)
        expect(ll.tail.value).eq(200)
    })

    it ('should return values for empty and non-emtpy list', () => {
        const val1 = ll.search(100) // empty list
        ll.addToHead(100)
        ll.addToHead(200)
        const val2 = ll.search(300)
        const val3 = ll.search(200)
        expect(val1).eq(null)
        expect(val2).eq(null)
        expect(val3).eq(200)
    })

    it ('should return indexe(s) for empty and non-empty list', () => {
        const val1 = ll.indexOf(88) // empty list)
        ll.addToTail(300)
        ll.addToTail(100)
        ll.addToTail(200)
        ll.addToTail(300)
        ll.addToTail(300)
        const val2 = ll.indexOf(400)
        const val3 = ll.indexOf(300)
        expect(val1).eql([])
        expect(val2).eql([])
        expect(val3).eql([0, 3, 4])
    })
})