function LinkedList () {
    this.head = null
    this.tail = null
}

function Node (value, next, previous) {
    this.value = value
    this.next = next
    this.previous = previous
}

LinkedList.prototype.addToHead = function (value) {
    const newNode = new Node(value, this.head, null)
    if (this.head) this.head.previous = newNode
    else this.tail = newNode // this is the only node in the list
    this.head = newNode
}

LinkedList.prototype.addToTail = function (value) {
    const newNode = new Node(value, null, this.tail)
    if (this.tail) this.tail.next = newNode
    else this.head = newNode
    this.tail = newNode
}

LinkedList.prototype.removeHead = function () {
    if (!this.head) return null
    const val = this.head.value
    this.head = this.head.next
    if (this.head) this.head.prev = null
    else this.tail = null // list is now empty
    return val
}

LinkedList.prototype.removeTail = function () {
    if (!this.tail) return null
    const val = this.tail.value
    this.tail = this.tail.previous
    if (this.tail) this.tail.next = null
    else this.head = null // list is now empty
    return val
}

LinkedList.prototype.search = function (searchValue) {
    let currentNode = this.head // head or tail
    while (currentNode) {
        if (currentNode.value === searchValue) return currentNode.value
        currentNode = currentNode.next
    }
    return null
}

LinkedList.prototype.indexOf = function (searchValue) {
    const indexes = []
    let currentIndex = 0
    let currentNode = this.head
    while (currentNode) {
        if (currentNode.value === searchValue) {
            indexes.push(currentIndex)
        }
        currentIndex++
        currentNode = currentNode.next
    }
    return indexes
}

module.exports = { LinkedList, Node }